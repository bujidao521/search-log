package com.log.search.constant;

/**
 * 全局常量
 *
 * @author chenfuqiang
 * 2022/7/1 10:32
 */
public class GlobalConstant {
    //默认的shell端口
    public static final int DEFAULT_SHELL_PORT = 22;
    public static final String DEFAULT_HOST_NAME = "默认实例";
}
