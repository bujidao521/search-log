package com.log.search.dto;

import lombok.Data;

@Data
public class LogSearchDto {
    private Long appId;

    private String keyword;

    private Long tailNum;
    private Long endNum;//扫码文件前n行；-1代表全文；
    private String upLine;//查找upLine行到endNum行
    private String fileName;
}
