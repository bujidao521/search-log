package com.log.search.dto;

import lombok.Data;

import java.util.List;

@Data
public class LogInfoDto {
    /**
     * 实例别名
     */
    private String hostName;

    /**
     * 主机ip
     */
    private String shellHost;

    /**
     * 关键词
     */
    private String keyword;

    /**
     * 日志
     */
    private List<String> logs;
}
