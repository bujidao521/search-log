package com.log.search.service;

import com.log.search.dto.FileDto;
import com.log.search.dto.LogInfoDto;
import com.log.search.dto.LogSearchDto;

import java.util.List;

public interface LogService {
    List<LogInfoDto>  searchLog(LogSearchDto searchDto) ;

    /**
     * 根据配置路径 +folder 查找应用下文件
     * @param appId 应用id
     * @param folder
     * @return 返回文件list
     */
    List<FileDto> searchFileName(long appId,String folder);

    /**
     * 查看配置下全部日志
     * @param searchDto
     * @return
     */
    List<LogInfoDto> allSearch(LogSearchDto searchDto);
}
